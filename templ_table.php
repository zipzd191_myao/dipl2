<?php
if (!isset($table_act)) 
{ // дія - перегляд, редагування, завантаження, видалення
 // $table_act = array(false,false,false,false);
 $table_act = '';// немає дій
} 

if (!isset($table_style)) { $table_style = "col_12 center"; } 
if (!isset($dopt)) { $dopt = ""; } 

$msg_table = "";

if (isset($mas_table))
{ 

$msg_table = "<div class=\"$table_style\">";
$msg_table .= "<table cellspacing=\"0\" cellpadding=\"0\" class=\"striped\" >";
// шапка таблиці
$msg_table .= "<thead><tr>";
$col = 0;
foreach ($mas_table[0] as $key => $value)
{
 $msg_table .= "<th>$value</th>";
 $col ++;
}
if ($table_act!='')
{ 
  $msg_table .= "<th>Дія</th>";
}
$msg_table .= "</tr></thead>";

if ( isset($maxt) && $maxt>0 )
{ // є дані для виведення
$msg_table .= "<tbody>";


 for ($i=1;$i<=$maxt;$i++)
 {
  $msg_table .= "<tr>";
   for ($j=0;$j<$col;$j++) 
   {
    // довні рядки відсікаємо
    $val = string_cut_full($mas_table[$i][$j],100,true);   
   
    $msg_table .= "<td>".$val."</td>";
   }
   
  if ($table_act!='')
  { 
    $msg_table .= "<td>";

    $msg_table .= isset($table_act[0]) && $table_act[0] 
      ? "<a href=\"?page=".$now_page."&act=1&n=".$mas_table[$i][$col]."$dopt\" title=\"Переглянути\"><span class=\"icon small\" data-icon=\"a\"></span></a> " : ""; // перегляд
    $msg_table .= isset($table_act[1]) && $table_act[1] 
      ? "<a href=\"?page=".$now_page."&act=2&n=".$mas_table[$i][$col]."$dopt\" title=\"Редагувати\"><span class=\"icon small\" data-icon=\"7\"></span></a> " : ""; //редагувати  7vw
//    $msg_table .= isset($table_act[2]) && $table_act[2] ? "<a href=\"?page=".$now_page."&act=3&n=".$mas_table[$i][$col]."$dopt\"><span class=\"icon small\" data-icon=\"D\"></span></a> " : ""; //додати матеріали /завантажити  ( Dp          p+  m- 
    $msg_table .= isset($table_act[2]) && $table_act[2] ? "<a href=\"?page=41&act=3&n=".$mas_table[$i][$col]."$dopt\" title=\"Завантажити зображення\"><span class=\"icon small\" data-icon=\"0\"></span></a> " : ""; //    
    $msg_table .= isset($table_act[3]) && $table_act[3] ? "<a href=\"?page=".$now_page."&act=4&n=".$mas_table[$i][$col]."$dopt\" title=\"Видалити\"><span class=\"icon small\" data-icon=\"T\"></span></a> " : ""; //видалити  xX
// 0 - картинка
    $msg_table .= isset($table_act[4]) && $table_act[4] ? "<a href=\"?page=".$now_page."&act=6&n=".$mas_table[$i][$col]."$dopt\" title=\"Відправити листа з паролем\"><span class=\"icon small\" data-icon=\"A\"></span></a> " : ""; //
    $msg_table .= isset($table_act[5]) && $table_act[5] ? "<a href=\"?page=4&grp=".$mas_table[$i][$col]."\" title=\"Перейти до тестів\"><span class=\"icon small\" data-icon=\"v\"></span></a> " : ""; // тести  
    $msg_table .= isset($table_act[6]) && $table_act[6] ? "<a href=\"?page=5&grp=".$mas_table[$i][$col]."\" title=\"Перейти в додаткові матеріали\"><span class=\"icon small\" data-icon=\"D\"></span></a> " : ""; //    дод матеріали
    $msg_table .= isset($table_act[7]) && $table_act[7] ? "<a href=\"?page=41&grp=".$mas_table[$i][$col]."\" title=\"Перейти до питань\"><span class=\"icon small\" data-icon=\"6\"></span></a> " : ""; //    відповіді
    $msg_table .= isset($table_act[8]) && $table_act[8] ? "<a href=\"?page=42&grp=".$mas_table[$i][$col]."\" title=\"Перейти до відповідей\"><span class=\"icon small\" data-icon=\".\"></span></a> " : ""; //    відповіді


    $msg_table .= "</td>";

  }
   
  $msg_table .= "</tr>";
 }
$msg_table .= "</tbody>";

}

$msg_table .= "</table>";
$msg_table .= "</div>";

}

if (isset($is_button_add) && $is_button_add)
{ // потрібна кнопка "додати"
 $msg_table_add = "<div class=\"clear\"></div>";
 $msg_table_add .= "<form method=\"post\" name=\"form\" action=\"?page=".$now_page."\">";
 $msg_table_add .= "<button class=\"medium\" name=\"add\"><span class=\"icon small\" data-icon=\"p\"></span> Додати</button>";

  if (isset($is_button_add_hidden) && $is_button_add_hidden!='')
  {
$msg_table_add .= "<input id=\"grp\" name=\"grp\" value=\"".$is_button_add_hidden."\" type=\"hidden\"  />";  
  }

  if (isset($is_button_add_hidden_t) && $is_button_add_hidden_t!='')
  {
$msg_table_add .= "<input id=\"t\" name=\"t\" value=\"".$is_button_add_hidden_t."\" type=\"hidden\"  />";  
  }

 $msg_table_add .= "</form>";

 $msg_table .=  $msg_table_add;
}


?>