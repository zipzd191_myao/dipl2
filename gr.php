<?php // content="text/plain; charset=utf-8"

$id_grp=0;
$id_t = 0;
$id_n = 0;
if (isset($_GET) && isset($_GET['grp']) && $_GET['grp']!='')
{
 $id_grp=$_GET['grp'];
 settype($id_grp, "integer");
}
if (isset($_GET) && isset($_GET['t']) && $_GET['t']!='')
{
 $id_t=$_GET['t'];
 settype($id_t, "integer");
}
if (isset($_GET) && isset($_GET['n']) && $_GET['n']!='')
{
 $id_n=$_GET['n'];
 settype($id_n, "integer");
}

 if ($id_n>0 && $id_t>0 && $id_grp>0)
{ // зчитуємо дані і будуємо графік

$data = $id_t.".06";
/////////////////////////////////////////////////////////////////

{ // немає запису - генеруємо         
$pi=3.14;                        
$sutki = 2*$pi/24; 

mt_srand((double)microtime()*1000000);

function func($a,$b,$c,$d,$e)
{
global $i, $sutki, $pi; 

  $r = 1;//mt_rand(0,$d)/100;
  $r = mt_rand(0,$d)/10;

  $f = $a+$b*sin($sutki*$i+$c*$pi/2+$e*$pi/20)+$r; 
  return($f);
}

$myquery_beg = "INSERT INTO datas ( idtrans, idtypedata,data,chas,value ) VALUES";
$myquery = "";
$koma = "";

for ($i=0;$i<24;$i++)
{ 
 $y=-1000000;
 $datax[]=$i;

// $ydata[] = 35+sin(2*$pi*$i*60*60/50+2.8*$pi/2);//
//35+sin($sutki*$i+2.8*$pi/2);
 
    switch($id_n)
    {
      case 1: // Температура верхніх шарів масла
              $y = func(80,3,2.75,10,1);
        break;
      case 2: // Температура масла на вході в охолоджувач
              $y = func(60,3,2.75,1,1);
        break; 
      case 3:  // Температура масла на виході з охолоджувача 	
              $y = func(35,1,2.8,1,1);
        break;
      case 4:  // Температура навколишнього середовища 	
        break;
      case 5:  // Струм, фаза A
        break;
      case 6:  // Струм, фаза B
        break;
      case 7:  // Струм, фаза C
        break;
      case 8:  // Струм, нейтраль
        break;
      case 9:  // Напруга, фаза A
//              $y = func(2,220,2.75,10,1);
        break;
      case 10:  // Напруга, фаза B
        break;
      case 11:  // Напруга, фаза C
        break;
    }  
 
 if ($y>-100000)
 {
   $ydata[] = $y;
   $myquery .= "$koma ($id_grp,$id_n,\"2018-06-$id_t\",\"$i\",\"$y\")";
   $koma = ", ";
 }

}          

if ($myquery!='')
{
 $myquery = $myquery_beg.$myquery ;
 //$result = mysqli_query($mylink,$myquery);

}                 

}




///////////////////////////////////////////////////////////////////

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_line.php');


// $ydata = array(11,13,15,10,8,5,3,6,8,10,12,7,5,4,1,6,9,11,3,4,5,6,7,10);
 //$datax = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23);
//ДОДАТОК Г Формат файлу даних у форматі JSON для тестування системи 



// вхідні дані
if (isset($ydata) && isset($datax))
{  // якщо є дані - виводимо,

// розмір зображення
$width=800;
$height=600;

// створюємо графічний об'єкт та визначаємо основні параметри для нього
$graph = new Graph($width,$height);
$graph->clearTheme();
$graph->SetScale('intlin'); // лінійна шкала
//$graph->SetShadow(); // тінь


$graph->ygrid->Show(true,true);
$graph->xgrid->Show(true,false);

// визначаємо поля навколо графіка та заголовки
$graph->SetMargin(40,20,40,40);
$graph->title->Set($data);
$graph->xaxis->title->Set('hours');

$graph->yaxis->title->SetFont( FF_FONT1 , FS_BOLD );
$graph->xaxis->title->SetFont( FF_FONT1 , FS_BOLD );

$graph->yaxis->SetColor('blue');

// створюємо лінійний графік
$lineplot=new LinePlot($ydata);
$lineplot->SetColor( 'blue' ); // колір лінії
$lineplot->SetWeight( 1 );   // товщина лінії
$lineplot->mark->SetType(MARK_UTRIANGLE);
$lineplot->mark->SetColor('blue'); // колір і заповнення маркерів
$lineplot->mark->SetFillColor('red');

$lineplot->value->Show();
// додааємо графік до граф. об'єкту 
$graph->Add($lineplot);

$graph->xaxis->SetTickLabels($datax);
$graph->xaxis->SetTextTickInterval(2);


// виводимо зображення
$graph->Stroke();

}
else
{// немає данних - виводимо текст

$im = @imagecreate (200, 100) or die ( "cannot create a new gd image.");
$background_color = imagecolorallocate ($im, 240, 240, 240);
$border_color = imagecolorallocate ($im, 50, 50, 50);
$text_color = imagecolorallocate ($im, 233, 14, 91);

imagerectangle($im,0,0,199,99,$border_color);
imagestring ($im, 10, 70, 40, "No data!", $text_color );
header ("content-type: image/png");
imagepng ($im);
}


}  // if ($id_n>0 && $id_t>0 && $id_grp>0)
?>